package dao;

import com.google.gson.Gson;
import modelo.Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author sergio
 */
public class ClienteDAOApi implements GenericDAO<Cliente> {


    @Override
    public Cliente find(int id) throws Exception {
        String lin, salida = "";
        Cliente cliente = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL("http://localhost:8080/clientes/" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                cliente = gson.fromJson(salida, Cliente.class);
            }
            con.disconnect();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return cliente;
    }

    @Override
    public List<Cliente> findAll() throws Exception {
        String lin, salida = "";
        Cliente[] clientes;
        List<Cliente> listClientes = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL("http://localhost:8080/clientes");
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }

                gson = new Gson();
                clientes = gson.fromJson(salida, Cliente[].class);
                listClientes = new ArrayList<Cliente>(Arrays.asList(clientes));
//				for (int i = 0; i < clientes.length; i++) {
//					listClientes.add(clientes[i]);
//				}
            }
            con.disconnect();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return listClientes;
    }

    @Override
    public boolean insert(Cliente t) throws Exception {
        Gson gson;
        try {
            gson = new Gson();
            String clienteJson = gson.toJson(t, Cliente.class);
            URL url;
            HttpURLConnection con;
                url = new URL("http://localhost:8080/clientes");
                con = (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                OutputStream os = con.getOutputStream();
                os.write(clienteJson.getBytes());
                os.flush();

                if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
                }

                // lectura del cuerpo de la respuesta
                String lin;
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                lin = br.readLine();
                int idCreado = Integer.parseInt(lin);
                System.out.println(idCreado);


                con.disconnect();
            System.out.println("¡Creado!");
            return  true;
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
        return  false;
    }

    @Override
    public boolean update(Cliente t) throws Exception {
        Gson gson;
        try {
            gson = new Gson();
            String clienteJson = gson.toJson(t, Cliente.class);
            URL url;
            HttpURLConnection con;

                url = new URL("http://localhost:8080/clientes/" + t.getId());
                con = (HttpURLConnection) url.openConnection();
                con.setDoOutput(true);
                con.setRequestMethod("PUT");
                con.setRequestProperty("Content-Type", "application/json");

                OutputStream os = con.getOutputStream();
                os.write(clienteJson.getBytes());
                os.flush();

                if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
                }
                con.disconnect();
            System.out.println("¡Modificado!");
            return true;
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL("http://localhost:8080/clientes/" + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            //con.setRequestProperty("Accept", "application/json");

            System.out.println(con.getResponseCode());
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                // System.out.println("Fallo leyendo recurso. Código de error: " +
                // con.getResponseCode());
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();
            return true;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public boolean delete(Cliente t) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL("http://localhost:8080/clientes/" + t.getId());
            System.out.println(url);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            //con.setRequestProperty("Accept", "application/json");

            System.out.println(con.getResponseCode());
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return  true;
    }

    @Override
    public List<Cliente> findByExample(Cliente t) throws Exception {
        return null;
    }
}
