package controlador;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import dao.ClienteDAOFile;
import dao.ClienteDAOJdbc;
import dao.ClienteDAOApi;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Color;
import modelo.Cliente;
import vista.Util;

public class ControllerCliente implements Initializable {

	private final static int MODO_NAVEGACION = 0;
	private final static int MODO_NUEVO_REGISTRO = 1;

	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnBorrar;
	@FXML
	private Button btnGuardar;
	@FXML
	private Button btnBuscar;
	@FXML
	private ToggleButton btnFiltrar;
	@FXML
	private TextField tfID;
	@FXML
	private TextField tfNombre;
	@FXML
	private TextField tfDireccion;
	@FXML
	private Label lblInfo;

	private ClienteDAOApi dao;
//	private ClienteDAOFile dao;
//	private ClienteDAOJdbc dao;

	private Cliente reg;
	private List<Cliente> registros;
	private int modo;
	private int posicionRegistro;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		tfID.setDisable(true);
		try {
			posicionRegistro = 0;
			dao = new ClienteDAOApi();
			registros = dao.findAll();
			if (registros.size() > 0) {
				reg = registros.get(posicionRegistro);
			} else {
				reg = null;
			}
			mostrarRegistro();
		} catch (Exception ex) {
			Util.mensajeExcepcion(ex, "Conectando/Consultando con la base de datos...");
			Platform.exit();
		}

	}

	// ******************************************************************************
	// ACCIONES ASOCIADAS A BOTONES
	// ******************************************************************************
	@FXML
	private void accionPrimero() {
		posicionRegistro = 0;
		mostrarRegistro();
	}

	@FXML
	private void accionAtras() {
		if (posicionRegistro > 0)
			posicionRegistro--;

		mostrarRegistro();
	}

	@FXML
	private void accionAdelante() {
		if (posicionRegistro < registros.size() - 1)
			posicionRegistro++;

		mostrarRegistro();
	}

	@FXML
	private void accionUltimo() {
		posicionRegistro = registros.size() - 1;

		mostrarRegistro();
	}

	@FXML
	private void accionBuscar() {
		TextInputDialog indicaId = new TextInputDialog("<id>");
		indicaId.setHeaderText("Indica id cliente a buscar:");
		indicaId.showAndWait();

		String respuesta = indicaId.getEditor().getText();
		if (!respuesta.isEmpty()) {

			try {
				int id = Integer.parseInt(respuesta);
				Cliente cli = dao.find(id);
				Alert d;
				String mensaje = "No encontrado";
				if (cli != null) {
					mensaje = cli.toString();
				}
				d = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.OK);
				d.setHeaderText("Búsqueda por cliente ID " + id);
				d.showAndWait();
			} catch (NumberFormatException e) {
				Util.mensajeExcepcion(e, "Id no es número válido...");
			} catch (Exception e) {
				Util.mensajeExcepcion(e, "Error buscando id...");
			}

		}

	}

	@FXML
	private void accionFiltrar() {
		if (btnFiltrar.isSelected()) {
			btnNuevo.setDisable(true);
			lblInfo.setTextFill(Color.RED);
			lblInfo.setText("Filtrado");
			Cliente cliMuestra = new Cliente();
			if (!tfNombre.getText().isEmpty())
				cliMuestra.setNombre(tfNombre.getText());
			if (!tfDireccion.getText().isEmpty())
				cliMuestra.setDireccion(tfDireccion.getText());
			try {
				registros = dao.findByExample(cliMuestra);
			} catch (Exception ex) {
				Util.mensajeExcepcion(ex, "Error consultando la bd...");
			}
		} else {
			btnNuevo.setDisable(false);
			lblInfo.setTextFill(Color.BLACK);
			lblInfo.setText("Filtrar");
			try {
				registros = dao.findAll();
			} catch (Exception ex) {
				Util.mensajeExcepcion(ex, "Error consultando la bd...");
			}
		}
		posicionRegistro = 0;
		mostrarRegistro();

	}

	@FXML
	private void accionNuevo() {
		modo = MODO_NUEVO_REGISTRO;
		tfID.setText("<autonum>");
		tfNombre.setText("");
		tfDireccion.setText("");
		tfNombre.requestFocus();
		btnBorrar.setDisable(true);

	}

	@FXML
	private void accionGuardar() {

		if (modo == MODO_NUEVO_REGISTRO) {
			Cliente c = new Cliente(tfNombre.getText(), tfDireccion.getText());
			try {
				dao.insert(c);
				registros.add(c);
				posicionRegistro = registros.size() - 1;
				mostrarRegistro();
				btnBorrar.setDisable(false);
				modo = MODO_NAVEGACION;
			} catch (Exception ex) {
				Util.mensajeExcepcion(ex, "Insertando registro...");
			}
		} else {
			try {
				Cliente c = new Cliente(Integer.parseInt(tfID.getText()), tfNombre.getText(), tfDireccion.getText());
				dao.update(c);
				registros.get(posicionRegistro).setNombre(tfNombre.getText());
				registros.get(posicionRegistro).setDireccion(tfDireccion.getText());
				modo = MODO_NAVEGACION;
			} catch (Exception ex) {
				Util.mensajeExcepcion(ex, "Actualizando registro...");
			}
		}

	}

	@FXML
	private void accionBorrar() {
		try {
			if (registros.size() > 0) {
				String mensaje = "¿Estás seguro de borrar el registro [" + tfID.getText() + "]?";
				Alert d = new Alert(Alert.AlertType.CONFIRMATION, mensaje, ButtonType.YES, ButtonType.NO);
				d.setTitle("Borrado de registro");
				d.showAndWait();
				if (d.getResult() == ButtonType.YES) {
					dao.delete(reg);
					registros.remove(posicionRegistro);
					if (posicionRegistro > 0)
						posicionRegistro--;
					mostrarRegistro();
				}
			}
		} catch (Exception ex) {
			Util.mensajeExcepcion(ex, "Borrando registro...");
		}
	}

	private void mostrarRegistro() {
		if (registros.size() > 0) {
			reg = registros.get(posicionRegistro);
		} else {
			reg = null;
		}
		lblInfo.setText("Registro " + (posicionRegistro + 1) + " de " + registros.size());
		if (reg != null) {
			tfID.setText(String.valueOf(reg.getId()));
			tfNombre.setText(reg.getNombre());
			tfDireccion.setText(reg.getDireccion());
		} else {
			tfID.setText(String.valueOf(0));
			tfNombre.setText("");
			tfDireccion.setText("");
		}
	}

}
